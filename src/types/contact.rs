
use rocket::serde::{Deserialize, Serialize};
use sqlx::{PgPool, Row};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Contact {
    pub id: ContactId,
    pub name: String,
    pub email: String,
    pub phone: String,
    pub address: String
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, Hash)]
pub struct ContactId(pub String);

impl Contact {
    pub async fn get_address_book_contacts(
        adres_book_id: u32, 
        pool: &PgPool) -> Result<Vec<Contact>, sqlx::Error>{
            let q = "SELECT * from contacts where  adress_book_id = $1";
            match sqlx::query(q)
            .bind(adres_book_id)
            .map(|row|Contact{
                id: ContactId(row.get("id")),
                name: row.get("name"),
                email: row.get("email"),
                phone: row.get("phone"),
                address: row.get("address")
            })
            .fetch_all(&*pool)
            .await{
                Ok(contacts) => Ok(contacts),
                Err(e) => Err(e)
            }

        }
}

