CREATE TABLE IF NOT EXISTS addressbooks (
id serial PRIMARY KEY,
address_book_name VARCHAR (255) NOT NULL UNIQUE,
created_on TIMESTAMP NOT NULL DEFAULT NOW()
);
