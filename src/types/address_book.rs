
use serde::{Deserialize, Serialize};
use crate::types::contact::Contact;


#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct AddressBook {
    pub id: AddressBookId,
    pub address_book_name: String,
    pub contacts: Vec<Contact>
}
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct AddressBookDTO {
    pub address_book_name: String
}

#[derive(Debug, Deserialize, Serialize, Clone, Eq, PartialEq, Hash)]
pub struct AddressBookId(pub String);




   

  