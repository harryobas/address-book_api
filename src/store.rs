use sqlx::{postgres::PgPool, Row};
use async_trait::async_trait;
use anyhow::Result;

use crate::types::{address_book::
    {AddressBook, AddressBookId, AddressBookDTO},
     contact::{Contact, ContactId}
    };

#[async_trait]
pub trait Store {
    async fn get_address_book_by_id(
        &self, 
        id: u32) -> Result<Option<AddressBook>, sqlx::Error>;

    async fn get_address_book_contacts(
        &self, 
        adres_book_id: u32) -> Result<Vec<Contact>, sqlx::Error>;

    async fn new_address_book(
        &self, 
        address_book: AddressBookDTO) -> Result<AddressBook, sqlx::Error>;

    }
pub struct DataStore<'a>{
    pool: &'a PgPool
}

impl<'a> DataStore<'a> {
    pub fn new(pool: &'a PgPool)  -> Self {
        Self{
            pool
        }
    }
}

#[async_trait]
impl<'a> Store for DataStore<'a> {
    async fn get_address_book_by_id(
        &self, 
        id: u32) -> Result<Option<AddressBook>, sqlx::Error>{
        let q = "SELECT * from adressbooks where id = $1";
        let contacts = self.get_address_book_contacts(id)
        .await.unwrap_or_else(|_|vec![]);

        match sqlx::query(q).bind(id)
        .map(|row|AddressBook{
            id: AddressBookId(row.get("id")),
            address_book_name: row.get("name"),
            contacts: contacts.clone()
        })
        .fetch_optional(&*self.pool)
        .await{
            Ok(address_book) => Ok(address_book),
            Err(e) => Err(e)
        }

    }

    async fn get_address_book_contacts(
        &self, 
        adres_book_id: u32) -> Result<Vec<Contact>, sqlx::Error>{
            let q = "SELECT * from contacts where  adress_book_id = $1";
            match sqlx::query(q)
            .bind(adres_book_id)
            .map(|row|Contact{
                id: ContactId(row.get("id")),
                name: row.get("name"),
                email: row.get("email"),
                phone: row.get("phone"),
                address: row.get("address")
            })
            .fetch_all(&*self.pool)
            .await{
                Ok(contacts) => Ok(contacts),
                Err(e) => Err(e)
            }
        }

    async fn new_address_book(
        &self, 
        address_book: AddressBookDTO) -> Result<AddressBook, sqlx::Error>{
            let q = "INSERT INTO addressbooks (name) VALUES ($1)
            RETURNING id, name";

            match sqlx::query(q).bind(address_book.address_book_name)
            .map(|row| AddressBook {
                id: AddressBookId(row.get("id")),
                address_book_name: row.get("name"),
                contacts: vec![]   
            })
            .fetch_one(&*self.pool)
            .await{
                Ok(address_book) => Ok(address_book),
                Err(e) => Err(e)
            }
    }

}