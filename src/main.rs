
mod types;
mod services;
mod api;
mod constants;

mod store;

use anyhow::Result;
use std::env;
use dotenv::dotenv;
use sqlx::postgres::PgPoolOptions;
use api::address_book_controller::{show, insert};

#[rocket::main]
 async fn main()-> Result<()> {

    dotenv().ok();
    let database_url = env::var("DATABASE_URL")?;

    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&database_url)
        .await?;


     let _rocket = rocket::build()
     .manage(pool)
     .mount("/api/addressbooks", rocket::routes![show, insert])
     .launch().await?;

     Ok(())
   
}