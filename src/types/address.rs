use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Address {
    pub id: AddressId,
    pub house_number: String,
    pub street_name: String,
    pub post_code: String,
    pub city: String,
    pub country: String 
}

#[derive(Debug, Deserialize, Serialize, Clone, Eq, PartialEq, Hash)]
pub struct AddressId(String);