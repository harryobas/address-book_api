
use rocket::response::status;
use rocket::State;
use rocket::http::Status;
use rocket::serde::json::Json;

use sqlx::PgPool;
use crate::types::{address_book::AddressBookDTO, response::Response};
use crate::store::*;
use crate::services::address_book_service::AddressBookService;


#[rocket::get("/<id>")]
pub async fn show(id: u32, pool: &State<PgPool>) 
-> status::Custom<Json<Response>> {
    let store = DataStore::new(pool);
    let response =  AddressBookService::find_address_book_by_id(id, store)
    .await;
   
   status::Custom(
       Status::from_code(response.status_code).unwrap(),
       Json(response.response)
   )
       
}

#[rocket::post("/", format = "application/json", data = "<address_book>" )]
pub async fn insert(address_book: Json<AddressBookDTO>, pool: &State<PgPool>) 
-> status::Custom<Json<Response>>{
    let store = DataStore::new(pool);
    let response = AddressBookService::create_address_book(address_book.into_inner(), store).await;

    status::Custom(
        Status::from_code(response.status_code).unwrap(),
        Json(response.response)
    )
}






    


