
use crate::types::address_book::AddressBookDTO;
use crate::types::response::{ResponseWithStatus, Response};
use crate::constants::message_constants;
use rocket::http::Status;
use crate::store::*;


pub struct AddressBookService{}

impl AddressBookService {
    pub async fn find_address_book_by_id<T: Store>(
        id: u32,
        store: T) -> ResponseWithStatus {

            if let Ok(address_book) = store.get_address_book_by_id(id)
            .await {
                if address_book.is_some(){
                    let address_book = address_book.unwrap();
                    return ResponseWithStatus{
                        status_code: Status::Ok.code,
                        response: Response { 
                            message: String::from(message_constants::MESSAGE_OK), 
                            data: serde_json::to_value(address_book).unwrap() 
                        }
                    }
                }else{
                    return ResponseWithStatus {
                        status_code: Status::NotFound.code,
                        response: Response { 
                            message: format!("address book with id {} not found", id),
                            data: serde_json::to_value("").unwrap() 
                        }
                    }
                }
            }
            ResponseWithStatus {
                status_code: Status::InternalServerError.code,
                response: Response { 
                    message: String::from(message_constants::MESSAGE_SERVER_ERROR), 
                    data: serde_json::to_value("").unwrap() 
                }
            }
        }

    pub async fn create_address_book<T: Store>(
        address_book: AddressBookDTO, 
        store: T) -> ResponseWithStatus{
            match store.new_address_book(address_book).await{
                Ok(address_book) => {
                    ResponseWithStatus{
                        status_code: Status::Created.code,
                        response: Response { 
                            message: String::from(message_constants::MESSAGE_CAN_NOT_INSERT_DATA), 
                            data: serde_json::to_value(address_book).unwrap()
                        }
                    }
                }

                Err(_e) => {
                    ResponseWithStatus{
                        status_code: Status::InternalServerError.code,
                        response: Response { 
                            message: String::from(message_constants::MESSAGE_SERVER_ERROR), 
                            data: serde_json::to_value("").unwrap()
                         }
                    }
                }
            }
        }

    }

   #[cfg(test)]
   mod tests {
       use super::*;
       use mockall::{predicate::*, mock};
       use anyhow::Result;
       use async_trait::async_trait;
       use crate::types::{address_book::{AddressBook, AddressBookId, AddressBookDTO}, contact::Contact};
       

       mock!{
           MockStore{}

           #[async_trait]
           impl Store for MockStore{
               async fn get_address_book_by_id(
                   &self, 
                   id: u32) -> Result<Option<AddressBook>, sqlx::Error>;

                async fn get_address_book_contacts(
                    &self,
                    adres_book_id: u32) -> Result<Vec<Contact>, sqlx::Error>;  
                
                async fn new_address_book(
                    &self,
                    address_book: AddressBookDTO) -> Result<AddressBook, sqlx::Error>;  
           }
       }

       macro_rules! aw {
           ($e: expr) => {
               tokio_test::block_on($e)
               
           };
       }

       #[test]
       fn test_find_address_book_by_id_with_correct_id(){
           let mut mock = MockMockStore::new();
           let address_book = AddressBook{
               id: AddressBookId("33".into()),
               address_book_name: "my book".into(),
               contacts: vec![]
           };

           mock.expect_get_address_book_by_id()
           .times(1)
           .with(eq(33))
           .returning(move |_| Ok(Some(address_book.clone())));

           aw!(
               AddressBookService::find_address_book_by_id(33, mock)
           );
       }

       #[test]
       fn test_find_address_book_by_id_with_incorrect_id(){
           let mut mock = MockMockStore::new();

           mock.expect_get_address_book_by_id()
           .times(1)
           .with(eq(3))
           .returning(|_| Ok(None));

           aw!(
               AddressBookService::find_address_book_by_id(3, mock)
           );
       }


   }

                
                    

                
            
           
        
               






